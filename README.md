<p><span style="color: #ff00ff; --darkreader-inline-color: #ff1aff; font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;" data-darkreader-inline-color=""><strong><img src="https://i.imgur.com/QmOpVQJ.png" width="940" height="470" /></strong></span></p>
<p><span style="color: #ff00ff; --darkreader-inline-color: #ff1aff; font-size: 14px; font-family: trebuchet ms, geneva, sans-serif;" data-darkreader-inline-color=""><strong>&nbsp;</strong></span></p>
<p><span style="font-size: 14px; font-family: trebuchet ms, geneva, sans-serif;"><img src="https://i.imgur.com/D9gI9A6.png" alt="About" width="197" height="56" /></span></p>
<p><span style="font-size: 14px; font-family: trebuchet ms, geneva, sans-serif;">&nbsp;</span></p>
<p><span style="font-size: 14px; font-family: trebuchet ms, geneva, sans-serif;"><strong>End Remastered aims to make your Minecraft experience more challenging by emphasizing on the adventurous side of the game. In brief, 11 new Ender Eyes are added along with a giant castle which replaces Vanilla Strongholds.</strong></span></p>
<p><span style="font-size: 14px; font-family: trebuchet ms, geneva, sans-serif;">&nbsp;</span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;">&nbsp;</span></p>
<p><span style="font-size: 14px; font-family: trebuchet ms, geneva, sans-serif;"><strong><span style="color: #ff00ff; --darkreader-inline-color: #ff1aff;" data-darkreader-inline-color=""><img src="https://i.imgur.com/rvI4D9Z.png" width="298" height="46" /></span></strong></span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;">&nbsp;</span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;">To activate the portal, you'll have to find the castle and in it find the portal, then place the 11 pearls in the frames of the Custom End Portal and activate it by right-clicking the End Creator (the grey frame) with a Powered Core.</span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;">&nbsp;</span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;">&nbsp;</span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><img src="https://i.imgur.com/rNmmRzI.png" alt="" width="224" height="57" /></span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;">&nbsp;</span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><strong>To learn more about the 11 custom eyes you'll need to open the portal to the End, click the button below:</strong></span></p>
<div class="spoiler">
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><em>1. Old Eye: </em>Found in Desert Pyramid</span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><em>2. Nether Eye: </em>Found in Nether Fortress</span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><em>3. Cold Eye: </em>Found in Igloo</span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><em>4. Rogue Eye: </em>Found in Jungle Pyramid</span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><em>5. Black Eye: </em>Found in Buried Chest</span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><em>6. Magical Eye: </em>Found in Woodland Mansion (1.16.1); Get it by killing Evoker (1.16.4)<em><br /></em></span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><em>7. Lost Eye: </em>Found in Mineshaft</span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><em>8. Corrupted Eye: </em>Found in Pillager Outpost</span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><em>9. Wither Eye: </em>Get it by killing The Wither</span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><em>10. Guardian Eye: </em>Get it by Killing Elder Guardian</span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><em>11. End Crystal Eye: </em>Get it by crafting it using the End Crystal, a new ore.</span></p>
</div>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;">&nbsp;</span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><strong>&nbsp;Our mod also adds a new structure in which the Custom End Portal will generate:</strong></span></p>
<div class="spoiler">
see Pictures at: https://www.curseforge.com/minecraft/mc-mods/endremastered
</div>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;">&nbsp;</span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;">&nbsp;</span></p>
<p><span style="font-size: 14px; color: #ff00ff; font-family: trebuchet ms, geneva, sans-serif;"><strong><img src="https://i.imgur.com/CrCZC1S.png" alt="" width="268" height="55" /></strong></span></p>
<p>&nbsp;</p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><strong>Click the spoiler button to see various crafting recipes for the custom Items we added to the game.</strong><strong><br /></strong></span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;">&nbsp;</span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><strong>The Core:</strong></span><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><br /></span></p>
<div class="spoiler"><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><img class="transparent" src="https://cdn.discordapp.com/attachments/473929078014214147/780572259571466270/unknown.png" alt="https://cdn.discordapp.com/attachments/473929078014214147/780572259571466270/unknown.png" /></span></div>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;">&nbsp;</span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><strong>The&nbsp; Powered Core:</strong></span><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><br /></span></p>
<div class="spoiler"><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><img class="transparent" src="https://cdn.discordapp.com/attachments/473929078014214147/780572047541403648/unknown.png" alt="https://cdn.discordapp.com/attachments/473929078014214147/780572047541403648/unknown.png" /></span></div>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;">&nbsp;</span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><strong>One of the custom eyes:</strong></span><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><br /></span></p>
<div class="spoiler"><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;">The End Crystal Eye</span><br /><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><img class="transparent" src="https://cdn.discordapp.com/attachments/473929078014214147/780572446591287376/unknown.png" alt="https://cdn.discordapp.com/attachments/473929078014214147/780572446591287376/unknown.png" /></span></div>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;">&nbsp;</span></p>
<p><span style="font-size: 14px; font-family: trebuchet ms, geneva, sans-serif;"><strong><span style="color: #ff00ff; --darkreader-inline-color: #ff1aff;" data-darkreader-inline-color=""><img src="https://i.imgur.com/2SMvWMZ.png" alt="" width="165" height="54" /><br /></span></strong></span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;">We always try to be aware of issues in End Remastered, if you experience a bug while playing our mod please report it in the comments or the "Issues" section to help us keep the experience fun and enjoyable for all players.<strong><br /></strong></span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;">&nbsp;</span><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><br /></span></p>
<p><span style="font-size: 14px; color: #00ffff; font-family: trebuchet ms, geneva, sans-serif;"><strong><span style="color: #000000;"><span style="font-size: 18px;"><img src="https://i.imgur.com/ToFfTsV.png" alt="" width="216" height="69" /></span></span></strong></span></p>
<p><span style="font-size: 14px; color: #800080; font-family: trebuchet ms, geneva, sans-serif;"><strong><span style="font-size: 18px;">End Remastered Let's Play - DECEMBER 17 Thru January 10</span></strong></span></p>
<p><span style="font-size: 14px; color: #00ffff; font-family: trebuchet ms, geneva, sans-serif;"><strong><span style="color: #000000;"><a href="https://www.twitch.tv/gilvoisard"><span style="font-size: 18px;">https://www.twitch.tv/gilvoisard</span></a></span></strong></span><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><br /></span></p>
<p>&nbsp;</p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><img src="https://i.imgur.com/hjIDM3P.png" alt="" width="246" height="63" /></span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><span style="color: #3366ff;"><strong>&nbsp;<a style="color: #3366ff;" href="https://discord.gg/D9cxayDNSP">CLICK HERE</a> </strong></span><strong>to&nbsp;</strong><strong>join our discord server to speak, chat, report problems o</strong><strong>r play with us :)</strong></span></p>