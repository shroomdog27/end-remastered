package com.ShteKen.endrem.config;


import com.ShteKen.endrem.EndRemastered;
import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.gui.widget.list.OptionsRowList;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.BooleanOption;
import net.minecraft.client.settings.SliderPercentageOption;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;

public final class ConfigUI extends Screen {

    private static final int TITLE_HEIGHT = 8;
    /**
     * Distance from top of the screen to the options row list's top
     *//**
     */
    private static final int OPTIONS_LIST_TOP_HEIGHT = 24;
    /**
     * Distance from bottom of the screen to the options row list's bottom
     */
    private static final int OPTIONS_LIST_BOTTOM_OFFSET = 32;
    /**
     * Height of each item in the options row list
     */
    private static final int OPTIONS_LIST_ITEM_HEIGHT = 25;

    /**
     * Width of a button
     */
    private static final int BUTTON_WIDTH = 200;
    /**
     * Height of a button
     */
    private static final int BUTTON_HEIGHT = 20;
    /**
     * Distance from bottom of the screen to the "Done" button's top
     */
    private static final int DONE_BUTTON_TOP_OFFSET = 26;

    private OptionsRowList optionsRowList;

    @Override
    protected void init() {
        //Create the Option RowList
        this.optionsRowList = new OptionsRowList(
                this.minecraft,
                this.width,
                this.height,
                OPTIONS_LIST_TOP_HEIGHT,
                this.height - OPTIONS_LIST_BOTTOM_OFFSET,
                OPTIONS_LIST_ITEM_HEIGHT
        );
        ///////////////////////////////////////////////////////////////////////////////////////////

        this.optionsRowList.addOption(new BooleanOption(
                "endrem.config.enable_ender_eyes",
                unused -> Config.ENABLE_ENDER_EYES.get(),
                (unused, newValue) -> Config.ENABLE_ENDER_EYES.set(newValue)
        ));

        this.optionsRowList.addOption(new BooleanOption(
                "endrem.config.enable_end_castle",
                unused -> Config.END_CASTLE_ENABLED.get(),
                (unused, newValue) -> Config.END_CASTLE_ENABLED.set(newValue)
        ));

        this.optionsRowList.addOption(new BooleanOption(
                "endrem.config.enable_end_gate",
                unused -> Config.END_GATE_ENABLED.get(),
                (unused, newValue) -> Config.END_GATE_ENABLED.set(newValue)
        ));
        this.optionsRowList.addOption(new SliderPercentageOption(
                "endrem.config.end_castle_distance",
                // Range: 0 to width of game window
                50<<4,

                750<<4,
                // This is an integer option, so allow whole steps only
                1.0F,
                // Getter and setter are similar to those in BooleanOption
                unused -> (double) (Config.END_CASTLE_DISTANCE.get() << 4),
                (unused, newValue) -> Config.END_CASTLE_DISTANCE.set(newValue.intValue() >> 4),
                // BiFunction that returns a string in format "<name>: <value>"
                (gs, option) -> new TranslationTextComponent("endrem.config.end_castle_distance", (int) option.get(gs))
        ));

        this.optionsRowList.addOption(new SliderPercentageOption(
                "endrem.config.end_gate_distance",
                // Range: 0 to width of game window
                50<<4,

                750<<4,
                // This is an integer option, so allow whole steps only
                1.0F,
                // Getter and setter are similar to those in BooleanOption
                unused -> (double) (Config.END_GATE_DISTANCE.get() << 4),
                (unused, newValue) -> Config.END_GATE_DISTANCE.set(newValue.intValue() >> 4),
                // BiFunction that returns a string in format "<name>: <value>"
                (gs, option) -> new TranslationTextComponent("endrem.config.end_gate_distance", (int) option.get(gs))
        ));
        this.optionsRowList.addOption(new SliderPercentageOption(
                "endrem.config.end_gate_size",
                // Range: 0 to width of game window
                10.0,

                50.0,
                // This is an integer option, so allow whole steps only
                1.0F,
                // Getter and setter are similar to those in BooleanOption
                unused -> (double) Config.END_GATE_SIZE.get(),
                (unused, newValue) -> Config.END_GATE_SIZE.set(newValue.intValue()),
                // BiFunction that returns a string in format "<name>: <value>"
                (gs, option) -> new TranslationTextComponent("endrem.config.end_gate_size", (int) option.get(gs))
        ));

        ///////////////////////////////////////////////////////////////////////////////////////////

        // Add the options row list as this screen's child. This allow player to click on it.
        this.children.add(this.optionsRowList);
          //Done Button
        this.addButton(new Button(
                (this.width - BUTTON_WIDTH),
                this.height - DONE_BUTTON_TOP_OFFSET,
                180,
                BUTTON_HEIGHT,
                new TranslationTextComponent("endrem.config.done_button"),
                button -> this.closeScreen()
        ));

        // Reset Button
        this.addButton(new Button(
                (this.width - BUTTON_WIDTH) - 260,
                this.height - DONE_BUTTON_TOP_OFFSET,
                180,
                BUTTON_HEIGHT,
                new TranslationTextComponent("endrem.config.reset_button"),
                button -> resetConfig()
        ));

    }

    public ConfigUI() {
        super(new TranslationTextComponent("endrem.config.title"));
    }


    /**
     * Draws this GUI on the screen.
     *
     * @param mouseX       horizontal location of the mouse
     * @param mouseY       vertical location of the mouse
     * @param partialTicks number of partial ticks
     */

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        this.renderDirtBackground(1);
        this.optionsRowList.render(matrixStack, mouseX, mouseY, partialTicks);
        drawCenteredString(matrixStack, this.font, this.title,this.width / 2, TITLE_HEIGHT, 0xFFFFFF);
        drawCenteredString(matrixStack, this.font, new TranslationTextComponent("endrem.config.restart_message"),this.width / 2, 185, 0xF0C828);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
    }

    private void resetConfig() {
        Config.END_GATE_DISTANCE.set(380);
        Config.END_GATE_SIZE.set(20);
        Config.END_CASTLE_DISTANCE.set(380);
        Config.ENABLE_ENDER_EYES.set(false);
        Config.END_CASTLE_ENABLED.set(true);
        Config.END_GATE_ENABLED.set(true);
        this.closeScreen();
        this.minecraft.displayGuiScreen(this);
    }
}