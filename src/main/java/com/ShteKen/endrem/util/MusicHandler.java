package com.ShteKen.endrem.util;

import net.minecraft.client.audio.BackgroundMusicSelector;
import net.minecraft.util.SoundEvent;

public class MusicHandler extends BackgroundMusicSelector{
    public MusicHandler(SoundEvent soundEvent, int minDelay, int maxDelay, boolean replaceCurrentMusic) {
        super(soundEvent, minDelay, maxDelay, replaceCurrentMusic);
    }
    public static final BackgroundMusicSelector CASTLE_MUSIC = new BackgroundMusicSelector(RegistryHandler.PORTAL_OPEN_SOUND.get(), 10, 20, true);
}



