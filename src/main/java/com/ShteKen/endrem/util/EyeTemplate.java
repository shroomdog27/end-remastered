package com.ShteKen.endrem.util;

import net.minecraft.block.Block;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class EyeTemplate {

    // get all the blocks around so you cannot place twice the same eye
    public static boolean IsFrameOfType(World worldIn, Block frame, BlockPos pos) {
        for (BlockPos blockPosMutable :
                BlockPos.getAllInBoxMutable(pos.add(9, 0, 9), pos.add(-9, 0, -9)))
        {
            if(worldIn.getBlockState(blockPosMutable).getBlock() == frame) {
                return true;
            }

        }
        return false;
    }
}

