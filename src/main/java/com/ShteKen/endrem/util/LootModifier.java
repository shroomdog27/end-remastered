package com.ShteKen.endrem.util;

import net.minecraft.loot.LootPool;
import net.minecraft.loot.TableLootEntry;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.LootTableLoadEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber
public class LootModifier {

    @SubscribeEvent
    public void addLoot(LootTableLoadEvent event) {
        if (event.getName().toString().equals("minecraft:chests/desert_pyramid")) {
            event.getTable().addPool(createPool("endrem:minecraft_chest/desert_pyramid"));
        } else if (event.getName().toString().equals("minecraft:chests/buried_treasure")){
            event.getTable().addPool(createPool("endrem:minecraft_chest/buried_treasure"));
        } else if (event.getName().toString().equals("minecraft:chests/abandoned_mineshaft")){
            event.getTable().addPool(createPool("endrem:minecraft_chest/abandoned_mineshaft"));
        } else if (event.getName().toString().equals("minecraft:chests/igloo_chest")){
            event.getTable().addPool(createPool("endrem:minecraft_chest/igloo_chest"));
        } else if (event.getName().toString().equals("minecraft:chests/jungle_temple")){
            event.getTable().addPool(createPool("endrem:minecraft_chest/jungle_temple"));
        } else if (event.getName().toString().equals("minecraft:chests/nether_bridge")){
            event.getTable().addPool(createPool("endrem:minecraft_chest/nether_bridge"));
        } else if (event.getName().toString().equals("minecraft:chests/pillager_outpost")){
            event.getTable().addPool(createPool("endrem:minecraft_chest/pillager_outpost"));
        }
    }

    private static LootPool createPool(String location){
        return LootPool.builder()
                .addEntry(TableLootEntry.builder(new ResourceLocation(location)).weight(1)).build();
    }
}
