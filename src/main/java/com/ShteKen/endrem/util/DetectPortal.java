package com.ShteKen.endrem.util;

import net.minecraft.block.Block;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class DetectPortal
{
    public static boolean getPortalCompleted(World worldIn, BlockPos end_creator_pos)
    {
        int activeFrameCounter = 0;
        for (BlockPos blockPos : BlockPos.getAllInBoxMutable(end_creator_pos.add(4, 0, 4), end_creator_pos.add(-4, 0, -4)))
        {

            for (Block frame : RegistryHandler.FRAME_ARRAY)
            {
                if (worldIn.getBlockState(blockPos).getBlock() == frame)
                {
                    activeFrameCounter += 1;
                }
            }
        }
        return activeFrameCounter == RegistryHandler.FRAME_ARRAY.length;
    }
}
