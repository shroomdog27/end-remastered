package com.ShteKen.endrem.events;


import com.ShteKen.endrem.config.Config;
import net.minecraft.item.EnderEyeItem;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber
public class ModEvents {

    // cancel the event aof activating the End Frame and locating the Stronghold

    @SubscribeEvent
    public static void DisableUsingEnderEyes(PlayerInteractEvent.RightClickBlock event) {
        if (event.isCancelable() && !Config.ENABLE_ENDER_EYES.get()) {
            if (event.getPlayer().inventory.getCurrentItem().getItem() instanceof EnderEyeItem) {
                event.setCanceled(true);
            }
        }
    }

    @SubscribeEvent
    public static void DisableThrowingEnderEyes(PlayerInteractEvent.RightClickItem event) {
        if (event.isCancelable() && !Config.ENABLE_ENDER_EYES.get()) {
            if (event.getPlayer().inventory.getCurrentItem().getItem() instanceof EnderEyeItem) {
                event.setCanceled(true);
            }
        }
    }
}