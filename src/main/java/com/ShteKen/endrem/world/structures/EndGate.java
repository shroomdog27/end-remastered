package com.ShteKen.endrem.world.structures;


import com.ShteKen.endrem.EndRemastered;
import com.ShteKen.endrem.config.Config;
import com.mojang.serialization.Codec;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SharedSeedRandom;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.util.math.SectionPos;
import net.minecraft.util.registry.DynamicRegistries;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.provider.BiomeProvider;
import net.minecraft.world.chunk.ChunkStatus;
import net.minecraft.world.chunk.IChunk;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import net.minecraft.world.gen.feature.jigsaw.JigsawManager;
import net.minecraft.world.gen.feature.structure.AbstractVillagePiece;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraft.world.gen.feature.structure.StructureStart;
import net.minecraft.world.gen.feature.structure.VillageConfig;
import net.minecraft.world.gen.feature.structure.*;
import net.minecraft.world.gen.feature.template.TemplateManager;
import net.minecraft.world.gen.settings.StructureSeparationSettings;

public class EndGate extends Structure<NoFeatureConfig> {
    BlockPos locatePos;

    public EndGate(Codec<NoFeatureConfig> codec) {
        super(codec);
    }

    @Override
    public  IStartFactory<NoFeatureConfig> getStartFactory() {
        return EndGate.Start::new;
    }

    @Override
    public GenerationStage.Decoration getDecorationStage() {
        return GenerationStage.Decoration.UNDERGROUND_STRUCTURES;
    }

    @Override
    protected boolean func_230363_a_(ChunkGenerator chunkGenerator, BiomeProvider biomeSource, long seed, SharedSeedRandom chunkRandom, int chunkX, int chunkZ, Biome biome, ChunkPos chunkPos, NoFeatureConfig noFeatureConfig) {
        // sets the minimum distance to 60% of the distance set in the config
        return (Math.sqrt(Math.pow(chunkX, 2) + Math.pow(chunkZ, 2)) > (0.6 * Config.END_CASTLE_DISTANCE.get()));
    }

    @Override
    public BlockPos func_236388_a_(IWorldReader world, StructureManager manager, BlockPos p_236388_3_, int radius, boolean skipExistingChunks, long seed, StructureSeparationSettings separationSettings) {
        int i = separationSettings.func_236668_a_();
        int j = p_236388_3_.getX() >> 4;
        int k = p_236388_3_.getZ() >> 4;
        int l = 0;

        for(SharedSeedRandom sharedseedrandom = new SharedSeedRandom(); l <= radius; ++l) {
            for(int i1 = -l; i1 <= l; ++i1) {
                boolean flag = i1 == -l || i1 == l;

                for(int j1 = -l; j1 <= l; ++j1) {
                    boolean flag1 = j1 == -l || j1 == l;
                    if (flag || flag1) {
                        int k1 = j + i * i1;
                        int l1 = k + i * j1;
                        ChunkPos chunkpos = this.getChunkPosForStructure(separationSettings, seed, sharedseedrandom, k1, l1);
                        IChunk ichunk = world.getChunk(chunkpos.x, chunkpos.z, ChunkStatus.STRUCTURE_STARTS);
                        StructureStart<?> structurestart = manager.getStructureStart(SectionPos.from(ichunk.getPos(), 0), this, ichunk);
                        if (structurestart != null && structurestart.isValid()) {
                            if (skipExistingChunks && structurestart.isRefCountBelowMax()) {
                                structurestart.incrementRefCount();
                                return this.locatePos;
                            }

                            if (!skipExistingChunks) {
                                return this.locatePos;
                            }
                        }

                        if (l == 0) {
                            break;
                        }
                    }
                }

                if (l == 0) {
                    break;
                }
            }
        }

        return null;
    }

    public static class Start extends StructureStart<NoFeatureConfig>  {
        public Start(Structure<NoFeatureConfig> structureIn, int chunkX, int chunkZ, MutableBoundingBox mutableBoundingBox, int referenceIn, long seedIn) {
            super(structureIn, chunkX, chunkZ, mutableBoundingBox, referenceIn, seedIn);
        }


        @Override
        public void func_230364_a_(DynamicRegistries dynamicRegistryManager, ChunkGenerator chunkGenerator, TemplateManager templateManagerIn, int chunkX, int chunkZ, Biome biomeIn, NoFeatureConfig config) {

            int x = (chunkX << 4) + 3;
            int z = (chunkZ << 4) + 3;
            final int nRoom = 16 + rand.nextInt(5);

            BlockPos genPosition = new BlockPos(x, 15, z);

            JigsawManager.func_242837_a(
                    dynamicRegistryManager,
                    new VillageConfig(() -> dynamicRegistryManager.getRegistry(Registry.JIGSAW_POOL_KEY)
                            .getOrDefault(new ResourceLocation(EndRemastered.MOD_ID, "end_gate/start_pool")),

                            Config.END_GATE_SIZE.get()), // Config file
                    AbstractVillagePiece::new,
                    chunkGenerator,
                    templateManagerIn,
                    genPosition,
                    this.components,
                    this.rand,
                    false,
                    false); // Prend la Y value de EndGetHeight au lieu du terrain matching



            this.components.forEach(piece -> piece.offset(0, 1, 0));
            this.components.forEach(piece -> piece.getBoundingBox().minY -= 1);

            this.recalculateStructureSize();

            if (this.getStructure() instanceof EndGate) {
                ((EndGate) this.getStructure()).locatePos = new BlockPos(
                        this.components.get(Math.min(nRoom, this.components.size())-1).getBoundingBox().minX,
                        this.components.get(Math.min(nRoom, this.components.size())-1).getBoundingBox().minY,
                        this.components.get(Math.min(nRoom, this.components.size())-1).getBoundingBox().minZ);
            }
        }
    }
}
